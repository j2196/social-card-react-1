import './App.css';
import CreateCard from './components/card';

function App() {
  return (
    <div className="App">
      <CreateCard />
    </div>
  );
}

export default App;
