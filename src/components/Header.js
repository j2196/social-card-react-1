import React from "react"

const HeaderCard = (props) => {
    return(
        <div className="header-main-container">
            <h1 className="main-heading">Dev</h1>
            <div>
                <h3>The Practical Dev <span>@ThePracticalDev.April 29</span></h3>
                <p>{props.contentTitle}</p>
                <p>{"{author: " + props.contentTitle + "}"}</p>
            </div>
        </div>
    )
}

export default HeaderCard