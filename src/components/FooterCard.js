import React, {Component} from "react";
import ImageComment from "./images/comment-solid.svg"
import ImageRetweet from "./images/retweet-solid.svg"
import ImageMessage from "./images/message-solid.svg"


class FooterCard extends Component {

  constructor(props) {
    super(props)

    this.state = {
      isLikedClass : false
    }

    this.isLiked = this.isLiked.bind(this);
  }

  isLiked() {
    if(this.state.isLikedClass) {
      this.setState({
        isLikedClass: false
      });
    } else {
      this.setState({
        isLikedClass: true
      });
    }
    
  }

  render() {
    return (
      
      <div className="follow-actions-main-container">
        <div className="follow-actions-container">
          <img src={ImageComment} alt=""/>
          <span className="comment-span">2</span>
        </div>
        <div className="follow-actions-container">
          <img src={ImageRetweet} alt=""/>
          <span className="retweet-span">47</span>
        </div>
        <div className="follow-actions-container">
          <i className= {this.state.isLikedClass ? 'bx bxs-heart likeButton addColor' : 'bx bxs-heart likeButton'}  id={this.props.iconId} onClick={this.isLiked}></i>
          <span className="like-span">190</span>
        </div>
        <div className="follow-actions-container">
          <img src={ImageMessage} alt=""/>
        </div>
      </div>
      

    );
  }
  
};

export default FooterCard;
