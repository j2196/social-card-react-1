import React from "react";
import LogoImage from "../logo.svg"

const ContentContainer = (props) => {
  return (
    <div>
      <div className="content-container">
        <div className="inside-heading-1-container">
          <h1 className="inside-heading-1">Dev</h1>
        </div>
        <div>
          <h1 className="inside-heading-2">{props.contentTitle}</h1>
        </div>
        <div className="author-container">
          <div className="author-name-tag">
            <h2 className="author-name-tag-heading">{props.name}</h2>
            <div className="logo-container">
              <img src={LogoImage} alt=""/>
            </div>
          </div>
        </div>
      </div>
      <div className="child-2-container">
        <h3 className="child-2-elements">{props.contentTitle}</h3>
        <p className="child-2-elements">
         {props.contentBody}
        </p>
        <span className="child-2-elements">dev.to</span>
      </div>
    </div>
  );
};

export default ContentContainer;
