import React, {Component} from "react";
import "./mystyles.css";
import HeaderCard from "./Header";
import ContentContainer from "./ContentContainer";
import FooterCard from "./FooterCard";


class CreateCard extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isDataFetched: false,
            postsData: []
        }
    }

    componentDidMount() {

        fetch("https://jsonplaceholder.typicode.com/posts")
        .then((response) => {
            return response.json()
        })
        .then((jsonData) => {
            this.setState({
                isDataFetched: true,
                postsData: jsonData
            });
            
        })
        .catch((error) => {
            console.log(error);
        })
    }

    render() {

        let jsxArray = [];

        if(this.state.isDataFetched) {
            jsxArray = this.state.postsData.map((object) => {
                return(
                    <div className="main-container" key={object.id}>
                        {/* The header Portion */}
                        <HeaderCard contentTitle={object.title}/>
                        {/* The inside Card */}
                        <ContentContainer contentTitle={object.title} contentBody={object.body}/>
                        {/* Footer Section */}
                        <FooterCard iconId={object.id}/>
                    </div>
                )
            })

            
        }

        return(
            
            <div>
                {jsxArray}
            </div>
         )
        
    }
   
}
    
    





export default CreateCard;
